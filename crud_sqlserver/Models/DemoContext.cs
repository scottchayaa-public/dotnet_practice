﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using crud_sqlserver.Models;

#nullable disable

namespace crud_sqlserver.Models
{
    public partial class DemoContext : DbContext
    {
        public DemoContext()
        {
        }

        public DemoContext(DbContextOptions<DemoContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
