# crud_sqlserver

 - 練習 webapi 開發
 - 使用 sql server 資料庫
 - 1 個 User table
 - 實作 UserController (Get, Post, Put, Delete)
 - DB Migration


# Install
```sh
# version: 5.0.302
dotnet --version

# 查看專案範本類型
dotnet new -l

# 建立專案: ASP.NET Core Web API
dotnet new webapi
```

# 偵錯環境

建立 launch.json > 選 .NET Core


# 測試專案

執行下列命令來信任 HTTPS 開發憑證：
```
dotnet dev-certs https --trust
```

# Scaffold 控制器

Controller 範本產生器

```sh
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer

dotnet tool install -g dotnet-aspnet-codegenerator
dotnet aspnet-codegenerator controller -name TodoItemsController -async -api -m TodoItem -dc TodoContext -outDir Controllers
```

- 新增 Scaffolding 所需的 `NuGet` 套件。
- 安裝 Scaffolding 引擎 (`dotnet-aspnet-codegenerator`)。
- Scaffold `TodoItemsController。`


# 程式自動更新功能

 - 安裝 [dotnet watch](https://www.nuget.org/packages/Microsoft.DotNet.Watcher.Tools/2.0.2)
 - .vscode/launch.json > `preLaunchTask` 改為 `watch`
 - Ctrl + F5 執行程式，之後 Controller 修改會自動重新 build code
 - 或 cli 執行 
```sh
# 程式啟動並監看
dotnet watch run dotnetcore-demo.csproj
```


# Entity Framework Core 工具參考-.NET Core CLI

https://docs.microsoft.com/zh-tw/ef/core/cli/dotnet

`dotnet ef` 可以安裝為全域或本機工具。 大部分的開發人員偏好 `dotnet ef` 使用下列命令安裝為通用工具：
```sh
# 安裝全域 ef 工具
dotnet tool install --global dotnet-ef

# 更新工具
dotnet tool update --global dotnet-ef
```


# Sql server 資料庫套件安裝 : Database First 模式

```
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Tools
```

scaffold 資料庫上所有的資料表，並將新檔案放在 [ 模型 ] 資料夾中。
```sh
dotnet ef dbcontext scaffold "Server=127.0.0.1;Database=demo;Trusted_Connection=false;User ID=sa;Password=Pass@word" Microsoft.EntityFrameworkCore.SqlServer -o Models -f
```

# Migration
```sh
# 根據 Models 建立 Migrations 檔案
dotnet ef migrations add Demo
# 根據 Migrations 更新 DB (資料庫連線需正確)
dotnet ef database update
```

# Docker

```
docker build -t crud_sqlserver .

docker run -e ASPNETCORE_ENVIRONMENT=Development crud_sqlserver
```

# 其他
```sh
# create gitignore file
dotnet new gitignore
```

# Reference
 - [教學課程：使用 ASP.NET Core 建立 Web API](https://docs.microsoft.com/zh-tw/aspnet/core/tutorials/first-web-api?view=aspnetcore-5.0&tabs=visual-studio)
 - [.NET 5 REST API Tutorial: 01 Getting Started](https://www.youtube.com/watch?v=bgk8N_rx1F4&ab_channel=JulioCasal)