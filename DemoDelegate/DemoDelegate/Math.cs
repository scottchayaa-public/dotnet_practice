﻿using System;

namespace DemoDelegate
{
    class Math
    {
        public Math()
        {

        }
        
        public void Echo()
        {
            Console.WriteLine("Hello Math.");
        }

        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Sub(int x, int y)
        {
            return x - y;
        }
    }
}
