﻿using System;

namespace DemoDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Math math = new Math();
            
            Action action = new Action(math.Echo);
            action.Invoke();

            var FuncAdd = new Func<int, int, int>(math.Add);
            var FuncSub = new Func<int, int, int>(math.Sub);

            Console.WriteLine($"FuncAdd.Invoke(2, 3) : {FuncAdd.Invoke(2, 3)}");
            Console.WriteLine($"FuncSub.Invoke(2, 3) : {FuncSub.Invoke(2, 3)}");
        }
    }
}
