using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using crud_mongodb.Models;

namespace crud_mongodb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly BookRepository _bookRepository;

        public BookController(BookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        // GET: api/books
        [HttpGet]
        public ActionResult<List<Book>> Get()
        {
            return _bookRepository.Get();
        }

        [HttpPost]
        public ActionResult Create(Book book)
        {
            return Ok(_bookRepository.Create(book));
        }
    }
}
