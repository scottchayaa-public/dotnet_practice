# crud_mongodb

 - 設定 MongoDB
 - 建立 MongoDB 資料庫
 - 定義 MongoDB 集合與結構描述
 - 從 Web API 執行 MongoDB CRUD 作業
 - 自訂 JSON 序列化


# Install
```sh
# version: 5.0.302
dotnet --version

# 查看專案範本類型
dotnet new -l

# 建立專案: ASP.NET Core Web API
dotnet new webapi
```

# 偵錯環境

建立 launch.json > 選 .NET Core


# 信任 HTTPS 開發憑證

```
dotnet dev-certs https --trust
```

# 套件工具

```sh
# mongodb 
dotnet add package MongoDB.Driver
```

# Docker

```sh
docker build -t crud_mongodb .
docker run -e ASPNETCORE_ENVIRONMENT=Development crud_mongodb
```

# 發佈到 IIS

```sh
dotnet publish -c Release

```

# 其他

 - visual studio 的 Debug 是看 `launchSettings.json` 中的 `applicationUrl`
 - vscode Debug 是看 `appsettings.json` 中的 `Urls`

# Reference
 - [使用 ASP.NET Core 與 MongoDB 建立 Web API](https://docs.microsoft.com/zh-tw/aspnet/core/tutorials/first-mongo-app?view=aspnetcore-5.0&tabs=visual-studio-code)
 - [Bookstore | Using ASP.NET Core 5.0 and MongoDB](https://www.youtube.com/watch?v=MNepwvCcKXA&ab_channel=AsielAlvarez)