using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace crud_mongodb.Models
{
    public class BookRepository
    {
        private readonly IMongoCollection<Book> _books;

        public BookRepository(IOptions<BookstoreDatabaseSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            var database = client.GetDatabase(settings.Value.DatabaseName);

            _books = database.GetCollection<Book>("Books");
        }
        
        public List<Book> Get() {
            return _books.Find(book => true).ToList();
        }

        public Book Get(string id) {
            return _books.Find<Book>(book => book.Id == id).FirstOrDefault();
        }

        public Book Create(Book book)
        {
            _books.InsertOne(book);
            return book;
        }

        public void Update(string id, Book bookIn) {
            _books.ReplaceOne(book => book.Id == id, bookIn);
        }

        public void Remove(Book bookIn) {
            _books.DeleteOne(book => book.Id == bookIn.Id);
        }

        public void Remove(string id) {
            _books.DeleteOne(book => book.Id == id);
        }
    }
}