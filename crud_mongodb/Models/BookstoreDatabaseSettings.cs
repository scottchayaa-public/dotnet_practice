﻿namespace crud_mongodb.Models
{
    public class BookstoreDatabaseSettings
    {
        public string DatabaseName { get; set; }
        public string BooksCollectionName { get; set; }
        public string ConnectionString { get; set; }
    }
}